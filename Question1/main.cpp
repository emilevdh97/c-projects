//This Program is used to calculate a person's BMI and displays the weight status
//according the BMI value, indicating whether the person is underweight,healthy,overweight or obese.
#include <iostream>

using namespace std;

//This struct is used to gather and return the data used to calculate the BMI.
struct Data{
    float weight;
    float height;
};

// prompts and returns a user's weight and height;
 Data getInputData(){
    Data data;
    cout << "Please enter you weight in Kilograms: ";
    cin >> data.weight;
    cout << "Please enter your height in Meters: ";
    cin >> data.height;
    return  data;
}
//Calculates the height squared and the Body mass index
 float calcBMI(float  weight, float  height)
{
    float heightSqr = height* height;
    float calculatedBmi = (weight/heightSqr);
    return calculatedBmi;
}
//Determines the weight status and displays the Bmi and the weight status message.
 void displayFitnessResults(float  calculatedBmi)
{
    string weightStatus;
    if(calculatedBmi<18.5)
        {
            weightStatus = "Underweight";
        }
    if(24.9>calculatedBmi || calculatedBmi>=18.5)
        {
            weightStatus = "Healthy";
        }
     if(29.9>calculatedBmi || calculatedBmi>25.0)
        {
            weightStatus = "Overweight";
        }
    if(calculatedBmi>30.0)
        {
            weightStatus = "Obese";
        }
    cout << "BMI\tWeight status" << endl;
    cout.setf(ios::fixed);
    cout.precision(1);
    cout << calculatedBmi<< '\t'<<weightStatus << endl;
}

int main()
{
    //Runs each function that handles the different parts of the program
    Data data = getInputData();
    float calculateBmi =calcBMI(data.weight,data.height);
    displayFitnessResults(calculateBmi);
}


