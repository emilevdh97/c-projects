//This program is used to generate a report based on the information given by a user
#include <iostream>

using namespace std;
//To neatly keep the information in one place a Sturct is used
//Each position in the arrays represents a subject
// results[0],code[0],distinction[0],symbol[0] will represent the subject english
struct Report{
    float results[6];
    float highest,lowest,average;
    string averageSymbol,averageCode;
    int code[6];
    char symbol[6];
    string distinction[6];
};
//To neatly keep the information in one place a Sturct is used
//A Student is an object and has a name,surname and schoolname
//A student has a object called the report.
struct Student{
    string name,surname,schoolName;
    Report report;
};


//This function is used to prompt the user to enter his/her details
//This function will then save information to a student object
void studentDetails(Student & student){
    cout << "Please key in your \nname: ";
    cin >> student.name >> student.surname;
    cin.get();
    cout << "Please key in the name of your \nschool: ";
    getline(cin,student.schoolName,'\n') ;


}
//This function is used to prompt the user to enter his/her subject marks
//This function will then save this information to an array belong to an object
void getMarks(Student & student){
    int counter = 0;
    float mark = 0;
    string subject[]={"English","Mathematics","Life Orientation","History","Computer literacy","Art"};

    while(counter !=6){
        cout << "Key in your marks for " << subject[counter] <<":\n";
        cin >> mark;

        if(mark >100 || mark < 0){
            cout << "Please enter a valid mark between 0 and 100 "<< endl;
        }else{
        student.report.results[counter] = mark;
        counter +=1;
        mark=0;
        }
    }
}
//This function will determine the maximum and minimum marks.
//This function will then print these values to the screen.
 void minMax(Report & report) {
    report.lowest=100;
    report.highest = 0;
    for(int i = 0; i <= 6;i++){
    if(report.results[i]< report.lowest){
        report.lowest = report.results[i];
    }
    if(report.results[i]> report.highest){
        report.highest = report.results[i];
    }
 }

    cout << "The highest mark was " << endl;
    cout << report.highest << "% The lowest mark was" <<endl;
    cout << report.lowest<<"%\n";
}
//This function is used to determine the code and symbol of each mark
//This function will print the code and symbol of each mark to console
void codeSymbol(Report & report){
 for (int i =0 ; i < 6 ; i++){
        if(report.results[i]>=80 && report.results[i]<=100){
            report.code[i] =7;
            report.symbol[i] ='A';
        }
        if(report.results[i]>=70 && report.results[i]<=79){
            report.code[i] =6;
            report.symbol[i] ='B';
        }
        if(report.results[i]>=60 && report.results[i]<=69){
            report.code[i] =5;
            report.symbol[i] ='C';
        }
        if(report.results[i]>=50 && report.results[i]<=59){
            report.code[i] =4;
            report.symbol[i] ='D';
        }
        if(report.results[i]>=40 && report.results[i]<=49){
            report.code[i] =3;
            report.symbol[i] ='E';
        }
        if(report.results[i]>=30 && report.results[i]<=39){
            report.code[i] =2;
            report.symbol[i] ='F';
        }
        if(report.results[i]>=0 && report.results[i]<=29){
            report.code[i] =1;
            report.symbol[i] ='FF';
        }

 }
  if(report.average>=80 && report.average<=100){
            report.averageCode ="7";
            report.averageSymbol ="A";
        }
        if(report.average>=70 && report.average<=79){
            report.averageCode ="6";
            report.averageSymbol ="B";
        }
        if(report.average>=60 && report.average<=69){
            report.averageCode ="5";
            report.averageSymbol ="C";
        }
        if(report.average>=50 && report.average<=59){
            report.averageCode ="4";
            report.averageSymbol ="D";
        }
        if(report.average>=40 && report.average<=49){
            report.averageCode ="3";
            report.averageSymbol ="E";
        }
        if(report.average>=30 && report.average<=39){
            report.averageCode ="2";
            report.averageSymbol ="F";
        }
        if(report.average>=0 && report.average<=29){
            report.averageCode  ="1";
            report.averageSymbol ="FF";
        }

    cout << "Subject\t\t\tMark\tSymbol\tCode"<<endl;
    cout << "English\t\t\t" <<report.results[0]<<'\t'<<report.symbol[0]<<'\t'<<report.code[0] <<endl;
    cout << "Mathematics\t\t"<<report.results[1]<<'\t'<<report.symbol[1]<<'\t'<<report.code[1] <<endl;
    cout << "Life Orientation\t"<<report.results[2]<<'\t'<<report.symbol[2]<<'\t'<<report.code[2] <<endl;
    cout << "History\t\t\t"<<report.results[3]<<'\t'<<report.symbol[3]<<'\t'<<report.code[3] <<endl;
    cout << "Computer literacy\t"<<report.results[4]<<'\t'<<report.symbol[4]<<'\t'<<report.code[4] <<endl;
    cout << "Art\t\t\t"<<report.results[5]<<'\t'<<report.symbol[5]<<'\t'<<report.code[5] <<endl;
}
//This function is used to calculate the average of the subjects and print average mark to screen with it's code and symbol
//This function will also call the code symbol function and determine the code and symbol for each mark.
void calcAverageYearMarks(Report & report){
    float  totalMarks = 0;
    const int NUM_OF_SUBJECTS = 6;
    for(int i = 0 ; i < NUM_OF_SUBJECTS ; i++){
        totalMarks += report.results[i];
    }
     report.average = float(totalMarks/NUM_OF_SUBJECTS);
    //Display Marks with code and Symbol
    codeSymbol(report);
    cout.precision(4);
    cout << "Average Year Mark: " << report.average << " with symbol " << report.averageSymbol << " and code " << report.averageCode << endl;
}
//This function will determine if the student has passed or failed the year according to his/her average mark
void PassOrFail (float & average){
    string passOrFail;
    if(average> 50){
        passOrFail = "Passed";
    }else{
    passOrFail="Failed";
    }
    cout<< "Outcome: " << passOrFail << endl;
}
//This function is used to determine whether the student has achieved a distinction
void awardDistinction(Report & report){
     string subject[]={"English","Mathematics","Life Orientation","History","Computer literacy","Art"};
    for(int i =0 ;i<6;i++ ){
    if(report.results[i]>75){
        report.distinction[i] = " Passed with Distinction";
    }else{
     report.distinction[i] = " Did not pass with Distinction";
    }
    cout << subject[i] <<report.distinction[i]<<endl;
    }
}
//This function is used to display the report and run the various function
void display(){
    Student student;
    //Prompt the student to enter their personal details and save it
    studentDetails(student);
    //Prompt the student enter their results and save it
    getMarks(student);
    //Display an open line
    cout << "\t"<<endl;
    //Display the report header
    cout << "***********************************************\n ";
    cout << "\t\t**** STUDENT ACADEMIC RECORD\n";
    cout << "This program inputs the learner marks of matric\n";
    cout << "level subjects and prints the student final report.\n";
    cout << "*********************************************** \n";
    cout << "****";
    //Display an open line
    cout << "\t"<<endl;
    //Display name,surname and school
    cout << "Name: " << student.name<<" "<< student.surname<<'\t'<<"School: "<<student.schoolName;
     //Display an open line
    cout <<"\t"<<endl;
    //Displays the Average year mark with the code and symbol
    //The average must be know in order to run codeSymbols.Code symbols in
    //inside of below function.
    calcAverageYearMarks(student.report);
    //Displays whether the student has passed or failed
    PassOrFail(student.report.average);
    //Display an open line
    cout << "\t"<<endl;
    //Displays Highest mark and the lowest mark.
    minMax(student.report);
    //Displays Footer
    cout <<"***************************************************";
    awardDistinction(student.report);
}
int main()
{
    display();
    return 0;
}



