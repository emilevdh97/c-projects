#include <iostream>
#include <string>
using namespace std;

string convertDate(string americanDate)
{
    string internationalDate,day;
    int positionOfFirstSpace =americanDate.find(" ",0);
    int lengthOfDay = 2;
   day = americanDate.substr((positionOfFirstSpace+1), lengthOfDay);
    americanDate.erase(positionOfFirstSpace, (lengthOfDay+1));
    internationalDate= day + " "+ americanDate;
}

int main()
{
    string americanDate;
    // Add the code for the main  function  here
    cout << "Please enter a date in american format: "<< endl ;
     getline(cin,americanDate,'\n');
    convertDate(americanDate);
return 0;
}
